import 'package:flutter/material.dart';

class AllMovies extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.black,
        appBar: AppBar(
          title: Text("M  O  V  I  E  -  I",
              style: TextStyle(fontSize: 28, fontWeight: FontWeight.bold)),
          backgroundColor: Colors.indigo[900],
          actions: <Widget>[
            IconButton(
              icon: Icon(
                Icons.search,
                color: Colors.white,
              ),
              onPressed: () {},
            ),
            PopupMenuButton(itemBuilder: (BuildContext context) {
              return [
                PopupMenuItem(child: Text('สำหรับมินิโปรเจค')),
              ];
            }),
          ],
          leading: Builder(
            builder: (BuildContext context) {
              return IconButton(
                icon: const Icon(Icons.menu),
                onPressed: () {},
              );
            },
          ),
        ),
        body: SingleChildScrollView(
          child: Container(
            width: double.infinity,
            child: Column(children: <Widget>[
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 20),
                child: Text(
                  'หนังและซีรีย์แนะนำ',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.network(
                      "https://0.soompi.io/wp-content/uploads/2021/08/23110511/squid-game.jpeg",
                      width: 150,
                      // height: 300,
                      fit: BoxFit.cover),
                  Container(
                    // height: 300,
                    width: MediaQuery.of(context).size.width * 0.4,
                    // color: Colors.amberAccent,
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            // mainAxisAlignment: MainAxisAlignment.center,
                            // crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Squid Game",
                                style: TextStyle(
                                    // fontSize: 24,
                                    ),
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              SizedBox(
                                  // width: 15,
                                  ),
                              Icon(
                                Icons.star,
                                color: Colors.yellow,
                                // size: 18,
                              ),
                              Text("8.2/10")
                            ],
                          ),
                          Text(
                            '   เรื่องย่อ',
                            style: TextStyle(fontSize: 16),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 5),
                            child: Text(
                                "  เรื่องราวสุดลึกลับของเหล่าผู้เข้าแข่งขันหลายร้อยชีวิตที่ประสบปัญหาทางการเงิน และต้องมาแข่งขันกันในเกมเด็กเล่นที่เต็มไปด้วยปริศนาลึกลับ"),
                          )
                        ],
                      ),
                    ),
                  )
                ],
              ),
              SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.network(
                      "https://m.media-amazon.com/images/I/71dY+AWVyJL._AC_SY679_.jpg",
                      width: 150,
                      // height: 300,
                      fit: BoxFit.cover),
                  Container(
                    // height: 300,
                    width: MediaQuery.of(context).size.width * 0.4,
                    // color: Colors.amberAccent,
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            // mainAxisAlignment: MainAxisAlignment.center,
                            // crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Fast 9",
                                style: TextStyle(
                                    // fontSize: 24,
                                    ),
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              SizedBox(
                                  // width: 15,
                                  ),
                              Icon(
                                Icons.star,
                                color: Colors.yellow,
                                // size: 18,
                              ),
                              Text("5.2/10")
                            ],
                          ),
                          Text(
                            '   เรื่องย่อ',
                            style: TextStyle(fontSize: 16),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 5),
                            child: Text(
                                "  ในภาคนี้จะเล่าถึงดอมโทเร็ตโต(วิน ดีเซล)ได้ออกมาใช้ชีวิตอย่างสงบพร้อมกับเลตตี้และไบรอันลูกชายแต่ก็ต้องมีเหตุให้ทีมซิ่งต้องกลับมารวมตัวอีกครั้ง"),
                          )
                        ],
                      ),
                    ),
                  )
                ],
              ),
              SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.network(
                      "https://m.media-amazon.com/images/M/MV5BMDIzODcyY2EtMmY2MC00ZWVlLTgwMzAtMjQwOWUyNmJjNTYyXkEyXkFqcGdeQXVyNDk3NzU2MTQ@._V1_.jpg",
                      width: 150,
                      // height: 300,
                      fit: BoxFit.cover),
                  Container(
                    // height: 300,
                    width: MediaQuery.of(context).size.width * 0.4,
                    // color: Colors.amberAccent,
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            // mainAxisAlignment: MainAxisAlignment.center,
                            // crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "The Truman Show",
                                style: TextStyle(
                                    // fontSize: 24,
                                    ),
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              SizedBox(
                                  // width: 15,
                                  ),
                              Icon(
                                Icons.star,
                                color: Colors.yellow,
                                // size: 18,
                              ),
                              Text("8.1/10")
                            ],
                          ),
                          Text(
                            '   เรื่องย่อ',
                            style: TextStyle(fontSize: 16),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 5),
                            child: Text(
                                "  The Truman Show เล่าเรื่องของ ทรูแมน (Jim Carrey) ชายหนุ่มที่มีชีวิตแสนธรรมดาในเมืองเล็กๆ บนเกาะอันสุขสงบ"),
                          )
                        ],
                      ),
                    ),
                  )
                ],
              ),
              SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.network(
                      "https://m.media-amazon.com/images/M/MV5BYTM3ZTllNzItNTNmOS00NzJiLTg1MWMtMjMxNDc0NmJhODU5XkEyXkFqcGdeQXVyODE5NzE3OTE@._V1_FMjpg_UX1000_.jpg",
                      width: 150,
                      // height: 300,
                      fit: BoxFit.cover),
                  Container(
                    // height: 300,
                    width: MediaQuery.of(context).size.width * 0.4,
                    // color: Colors.amberAccent,
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            // mainAxisAlignment: MainAxisAlignment.center,
                            // crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Kingsman",
                                style: TextStyle(
                                    // fontSize: 24,
                                    ),
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              SizedBox(
                                  // width: 15,
                                  ),
                              Icon(
                                Icons.star,
                                color: Colors.yellow,
                                // size: 18,
                              ),
                              Text("7.7/10")
                            ],
                          ),
                          Text(
                            '   เรื่องย่อ',
                            style: TextStyle(fontSize: 16),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 5),
                            child: Text(
                                "  เด็กหนุ่มวัยรุ่นที่มีระดับสติปัญญาเป็นเลิศ ผลกการเรียนยอดเยี่ยม สู่การเลือกเส้นทางเดินของชีวิตด้านมืดเยี่ยงอาชญากรกับการค้ายาเสพติด"),
                          )
                        ],
                      ),
                    ),
                  )
                ],
              ),
              SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.network(
                      "https://m.media-amazon.com/images/M/MV5BMTU2NjA1ODgzMF5BMl5BanBnXkFtZTgwMTM2MTI4MjE@._V1_.jpg",
                      width: 150,
                      // height: 300,
                      fit: BoxFit.cover),
                  Container(
                    // height: 300,
                    width: MediaQuery.of(context).size.width * 0.4,
                    // color: Colors.amberAccent,
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            // mainAxisAlignment: MainAxisAlignment.center,
                            // crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "John Wick",
                                style: TextStyle(
                                    // fontSize: 24,
                                    ),
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              SizedBox(
                                  // width: 15,
                                  ),
                              Icon(
                                Icons.star,
                                color: Colors.yellow,
                                // size: 18,
                              ),
                              Text("7.4/10")
                            ],
                          ),
                          Text(
                            '   เรื่องย่อ',
                            style: TextStyle(fontSize: 16),
                          ),
                          Text(
                              "  นักฆ่ามือ1ของวงการที่ตัดสินใจล้างมือจากโลกมืดและเริ่มต้นใช้ ชีวิตใหม่อย่างเงียบสงบหลังจากภรรยาเสียชีวิตแต่ชีวิตของเขาไม่เป็นอย่างที่คิด")
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ]),
          ),
        ));
  }
}
