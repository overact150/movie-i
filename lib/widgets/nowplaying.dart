import 'package:flutter/material.dart';
import 'package:movie_app_flutter/utils/text.dart';

class NowPlayingMovies extends StatelessWidget {
  final List nowplaying;

  const NowPlayingMovies({Key key, this.nowplaying}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 15),
      padding: EdgeInsets.all(10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          modified_text(
            text: 'Now Playing Movies | กําลังเล่นอยู่ขณะนี้',
            size: 24,
          ),
          SizedBox(height: 10),
          Container(
            margin: EdgeInsets.only(top: 10),
              height: 350,
              child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: nowplaying.length,
                  itemBuilder: (context, index) {
                    return Container(
                      width: 200,
                      child: Column(
                        children: [
                          Container(
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                image: NetworkImage(
                                    'https://image.tmdb.org/t/p/w500' +
                                        nowplaying[index]['poster_path']),
                              ),
                            ),
                            height: 260,
                          ),
                          SizedBox(height: 5),
                          Container(
                            margin: EdgeInsets.only(top: 10),
                            child: modified_text(
                                size: 12,
                                text: nowplaying[index]['title'] != null
                                    ? nowplaying[index]['title']
                                    : 'Loading'),
                          )
                        ],
                      ),
                    );
                  }))
        ],
      ),
    );
  }
}
